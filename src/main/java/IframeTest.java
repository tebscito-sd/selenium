import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class IframeTest {
    private WebDriver driver;
    private String baseUrl = "https://www.dezlearn.com/testingpage/";

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
    }

    @Test
    public void findIframes() {
        //Finding all iframe tags on a web page
        List<WebElement> elements = driver.findElements(By.tagName("iframe"));
        int numberOfTags = elements.size();
        System.out.println("Number of Iframes of the Web Page: " + numberOfTags);
    }

    @Test
    public void switchIframes() throws InterruptedException {

        //get iframe 1
        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated((By.id("do-it-iframe"))));
        driver.switchTo().frame("do-it-iframe");
        driver.findElement(By.className("search-field")).sendKeys("selenium");
        driver.findElement(By.className("search-submit")).click();

        //back to main page
        driver.switchTo().defaultContent();
        Thread.sleep(2000);

        //get iframe 2
        wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.visibilityOfElementLocated((By.id("contact-iframe"))));

        driver.switchTo().frame("contact-iframe");
        driver.findElement(By.id("ninja_forms_field_11")).sendKeys("Juan Carlos");
        driver.findElement(By.id("ninja_forms_field_12")).sendKeys("juan.carlos@gmail.com");
        driver.findElement(By.id("ninja_forms_field_13")).sendKeys("This is a test sample text");

        //back to main page
        driver.switchTo().defaultContent();
    }

    @Test(enabled = false)
    public void frameTest() {
        driver.get("https://demoqa.com/frames");
        WebElement frame = driver.findElement(By.id("frame1"));
        driver.switchTo().frame(frame);
        String frameText = driver.findElement(By.id("sampleHeading")).getText();
        System.out.println(frameText);
    }

    @Test(enabled = false)
    public void nestedFrameTest() {
        driver.get("https://demoqa.com/nestedframes");
        int countIframesInPage = driver.findElements(By.tagName("iframe")).size();
        System.out.println("Number of Frames on a Page:" + countIframesInPage);

        WebElement frame1 = driver.findElement(By.id("frame1"));
        driver.switchTo().frame(frame1);
        WebElement frame1Element= driver.findElement(By.tagName("body"));
        String frame1Text=frame1Element.getText();
        System.out.println("Frame1 is :"+frame1Text);

        int countIframesInFrame1 = driver.findElements(By.tagName("iframe")).size();
        System.out.println("Number of iFrames inside the Frame1:" + countIframesInFrame1);

        driver.switchTo().frame(0);

        int countIframesInFrame2 = driver.findElements(By.tagName("iframe")).size();
        System.out.println("Number of iFrames inside the Frame2:" + countIframesInFrame2);
    }

    @AfterClass
    public void close() throws InterruptedException {
        Thread.sleep(2000);
        driver.close();
    }

}
