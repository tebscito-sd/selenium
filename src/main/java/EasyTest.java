import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EasyTest {
    private WebDriver driver;
    private String baseUrl = "https://www.seleniumeasy.com";

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get(baseUrl);
        driver.manage().window().maximize();
    }

    @Test(enabled = false)
    public void dropDownTest() {
        driver.get(baseUrl + "/test/basic-select-dropdown-demo.html");
        Select objSelect = new Select(driver.findElement(By.cssSelector("select[class='form-control']")));
        objSelect.selectByVisibleText("Sunday");
    }

    @Test(enabled = false)
    public void dropDownTest_multiSelect() {
        driver.get(baseUrl + "/test/basic-select-dropdown-demo.html");
        Select objSelect = new Select(driver.findElement(By.name("States")));
        objSelect.selectByVisibleText("California");
        objSelect.selectByVisibleText("Florida");
    }

    @Test(enabled = false)
    public void radioButtonTest() {
        driver.get(baseUrl + "/test/basic-radiobutton-demo.html");
        String message = "Radio button 'Female' is checked";
        WebElement maleRadioButton = driver.findElement(By.cssSelector("input[type='radio'][value='Male'][name='optradio']"));
        WebElement femaleRadioButton = driver.findElement(By.cssSelector("input[type='radio'][value='Female'][name='optradio']"));
        WebElement buttonCheck = driver.findElement(By.cssSelector("button[type='button'][id='buttoncheck']"));
        femaleRadioButton.click();
        buttonCheck.click();

        //Check expected behaviour
        String actualMessage = driver.findElement(By.cssSelector("p[class='radiobutton']")).getText();
        Assert.assertEquals(message, actualMessage, "The expected message was not correct");
    }

    @Test(enabled = false)
    public void radioButtonTest_multiSelect() {
        //TODO
    }

    @Test(enabled = false)
    public void alert_accept() throws InterruptedException {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.cssSelector("div[class=\"panel panel-primary\"] button[class=\"btn btn-default\"]"))
                .click();
        Thread.sleep(3000);
        driver.switchTo().alert().accept();
        System.out.println();
    }

    @Test(enabled = false)
    public void alert_cancel() throws InterruptedException{
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class=\"btn btn-default btn-lg\"][text()=\"Click me!\"]"))
                .click();
        Thread.sleep(3000);
        driver.switchTo().alert().dismiss();
    }

    @Test(enabled = false)
    public void alert_setMessage() throws InterruptedException {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class=\"btn btn-default btn-lg\"][text()=\"Click for Prompt Box\"]"))
                .click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Juan Carlos");
        alert.accept();
    }

    @Test(enabled = false)
    public void alert_getMessage() throws InterruptedException {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class=\"btn btn-default btn-lg\"][text()=\"Click for Prompt Box\"]"))
                .click();
        Thread.sleep(3000);
        Alert alert = driver.switchTo().alert();
        String message = alert.getText();
        alert.dismiss();
        System.out.println(message);
    }

    @Test(enabled = true)
    public void alert_getMessage_wait() throws InterruptedException {
        driver.get(baseUrl + "/test/javascript-alert-box-demo.html");
        driver.findElement(By.xpath("//button[@class=\"btn btn-default btn-lg\"][text()=\"Click for Prompt Box\"]"))
                .click();
        Thread.sleep(3000);
        WebDriverWait wait = new WebDriverWait(driver, 3);
        Alert alert = wait.until(ExpectedConditions.alertIsPresent());
        String message = alert.getText();
        alert.dismiss();
        System.out.println(message);
    }

    @AfterClass
    public void close() throws InterruptedException {
        Thread.sleep(2000);
        driver.close();
    }

}
