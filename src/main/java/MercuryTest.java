import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class MercuryTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver" , "chromedriver");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://demo.guru99.com/selenium/newtours");
        driver.manage().window().maximize();
    }

    @Test
    public void checkSiteName() {
        String expectedTitle = "Welcome: Mercury Tours";
        String actualTitle = "";
        actualTitle = driver.getTitle();

        if(actualTitle.contentEquals(expectedTitle)) {
            System.out.println("Test Passed");
        } else {
            System.out.println("Test Failed");
        }

        Assert.assertEquals(actualTitle, expectedTitle, "Title page doesn't show as expected");
    }

    @AfterClass
    public void close() {
        driver.close();
    }
}
